import json

dataList = []

with open('input-data.txt') as reader:
    counter=0
    
    for line in reader.read().replace(' ', '').split('\n'):
        
        lineData = line.rstrip().split('|')
        if lineData[0] != '':
            
            print_flag = False
            severity = ""

            red_high_limit = lineData[2]
            yellow_high_limit =  lineData[3]
            yellow_low_limit =  lineData[4]
            red_low_limit =  lineData[5]
            
            timestamp = lineData[0]
            raw_value = lineData[6]
            component = lineData[7]
            satellite_id = lineData[1]
           

            if (float(raw_value) > float(red_high_limit)):
                severity = "RED HIGH"
                print_flag = True
                #break
            elif (float(raw_value) > float(yellow_high_limit)):
                severity = "YELLOW HIGH"
                print_flag = True
                #break
            elif (float(raw_value) < float(red_low_limit)):
                severity = "RED LOW"
                print_flag = True
            elif (float(raw_value) < float(yellow_low_limit)):
                severity="YELLOW LOW"
                print_flag = True
            else:
                severity= "None"
                print_flag = False           

            if print_flag:
                dataList.append({
                    "satelliteId":          satellite_id,
                    "severity":             severity,
                    "component":            component,
                    "timestamp":            timestamp,
                })
            severity = ""

out_json = json.dumps(dataList, indent=4)
print(out_json)




